package codesmells

import (
	"fmt"
	"math/rand"
)

func DuplicateString() {
	fmt.Println("This should be constant")
	fmt.Println("This should be constant")
	fmt.Println("This should be constant")
	fmt.Println("This should be constant")
}

func EmptyFunc() {

}

func Fun1() (x, y int) {
	a, b := 1, 2
	b, a = a, b
	return a, b
}

func Fun2() (x, y int) { // Noncompliant; fun1 and fun2 have identical implementations
	a, b := 1, 2
	b, a = a, b
	return a, b
}

func ConditionalStructure() {
	// FIXME
	num := rand.Intn(3)
	switch num {
	case 0:
		Fun1()
	case 1:
		Fun2()
	case 2:
		Fun1()
	}

	if !(num == 4) || !(num+1 == 3) && true {
		//empty
		fmt.Println("This should be constant")
	}

	if num+1 == 4 {

		if num+4 == 5 {

			for i := 1; i <= 10; i++ {

				if num == 1 {
					if num == 2 {
						fmt.Println("This should be constant")
					}
					return
				}
			}
		}
	}

}

func ManyParams(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 int) {
	/*  */
}
